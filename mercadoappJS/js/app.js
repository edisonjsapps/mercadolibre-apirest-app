window.addEventListener('load', () => {

    // Variables
    let productos = [];
    let evento = '';
    let palabra = '';

    // Buscador
    let search = document.getElementById('buscador');
    search.addEventListener('keyup', (resp => {
        evento = resp;
        palabra = search.value;
        obtenerProductos(evento, palabra);
    }));

    // Boton Buscador
    let buscarboton = document.getElementById('buscarbtn');
    buscarboton.addEventListener('click', (resp => {
        evento = resp;
        palabra = search.value;
        obtenerProductos(evento, palabra);
    }));

    // Funcion que llama el api para traer el listado de 4 productos
    function obtenerProductos(evento, palabra) {
        if (evento.keyCode == 13 || evento.type == 'click') {
            fetch(`http://localhost:3000/api/items/search/${palabra}`).then(response => response.json()).then(resultado => {
                productos = resultado;
                let cajas = '';
                //   Construye el breadcrumb
                cajas += `<div class="container-producto">
                    <br>
                    <span" class="bread">Breadcrumb / ${productos.categorias}</span>
                    <br>
                    <br>`
                    // Contruye dinamicamente los productos
                for (const item of productos.items) {
                    cajas += `<div class="card mb-3" >
                        <div class="row no-gutters">
                            <div class="col-md-3">
                                <img class="img-product" src="${item.picture}" class="img-prod card-img" alt="...">
                            </div>
                            <div class="col-md-9">
                                <div class="card-body">
                                    <h3 class="card-title"> $ ${item.price.price} <img width="20px" src="assets/freeShipping.png" alt=""></h3>
                                    <p class="card-text">${item.title}</p>
                                    <hr>
                                    <p class="card-text"><small class="text-muted">Condición: <span class="condicion">${item.condition}</span></small></p>
                                    <a id="detalle" class="btn btn-primary" href="producto.html?detalle=${item.id}">Ver detalle</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`
                }
                // rellena dinamicamente el contenedor con los productos.
                document.getElementById('contenedor').innerHTML = cajas;

            });
        }

    }

});