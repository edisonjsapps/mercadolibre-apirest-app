window.addEventListener('load', () => {

    // obtiene los parametros de la url para hacer la busqueda por Id
    let parametro = window.location.search;
    const urlParams = new URLSearchParams(parametro).get('detalle');

    // Llamado al api por los detalles del producto
    fetch(`http://localhost:3000/api/item/${urlParams}`)
        .then(response => response.json())
        .then(resp => {
            detalle = resp.item;
            let cajas = '';
            // Contruye dinamicamente los detalles del producto
            cajas += `        <div class="container-detalle">
                <br><br>
                <div class="card mb-3">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="${detalle.picture}" class="card-img" alt="...">
                            <br><br>
    
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <p class="card-text condicion">Condiciones: ${detalle.condition} / Vendidos: ${detalle.sold_quantity}</p>
                                <p class="card-text"><strong>${detalle.title}</strong></p>
                                <h2 class="card-title precio">${detalle.price.price} <small class="currency">${detalle.price.currency}</small></h2>
                                <hr>
                                <button class="btn btn-primary">Comprar</button>
    
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="descripcion">
                            <h5 class="card-title">Descripción del producto</h5>
                            <hr>
                            <p class="card-text"><small class="text-muted">${detalle.description}</small></p>
                        </div>
                    </div>
                </div>
            </div>`

            // rellena dinamicamente el contenedor con los detalles del producto.
            document.getElementById('contenedor').innerHTML = cajas;
        })
});