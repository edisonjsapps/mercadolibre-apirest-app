import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { ArticuloComponent } from './articulo/articulo.component';

const routes: Routes = [
  {path:'home',component: BusquedaComponent },
  {path:'articulo/:id',component: ArticuloComponent },
  {path:'**', pathMatch:'full', redirectTo:'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
