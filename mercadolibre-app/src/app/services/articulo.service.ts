import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ArticuloService {

  constructor( private _http:HttpClient) { }
  obtenerArticulo(code:string){
    let url = `http://localhost:3000/api/item/${code}`;
    return this._http.get(url).pipe(map(resp => resp));
  }

}
