import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BusquedaService {

  constructor(private _http:HttpClient) { }

 obtenerProductos(productos:string){
    let url = `http://localhost:3000/api/items/search/${productos}`; 
    return this._http.get(url).pipe(map(resp => resp));

  }



}
