import { Component, OnInit } from '@angular/core';
import { BusquedaService } from '../services/busqueda.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent implements OnInit {

  productos:[]=[];
  categorias:string;

  constructor(private buscar:BusquedaService) { }

  ngOnInit(): void {
  }



  buscarProductos(elemento:string){
    this.buscar.obtenerProductos(elemento).subscribe((data:any) => {
      if(data){
        this.categorias=data.categorias;
        this.productos=data.items;
        console.log(this.productos);
      }
    });
  }



}
