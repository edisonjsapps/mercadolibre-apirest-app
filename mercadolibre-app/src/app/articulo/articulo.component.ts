import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticuloService } from '../services/articulo.service';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.css']
})
export class ArticuloComponent implements OnInit {
  codigo:string;
  articulo:any[]=[];


  constructor(private _ac:ActivatedRoute, private _as:ArticuloService) {
    this._ac.params.subscribe(resp => {
      this.codigo = resp.id;
      this.traerArticulo(this.codigo);
});
  }

  ngOnInit(): void {
  }


  traerArticulo(code:string){
    this._as.obtenerArticulo(code).subscribe((resp:any) => {
      this.articulo=resp.item
      return this.articulo;
      
    });
  }


}
