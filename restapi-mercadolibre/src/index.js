const express = require('express');
const app = express();
const morgan = require('morgan');
const http = require('http');
const { response } = require('express');

// Configurar cabeceras y cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');

    // authorized headers for preflight requests
    // https://developer.mozilla.org/en-US/docs/Glossary/preflight_request
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();

    app.options('*', (req, res) => {
        // allowed XHR methods  
        res.header('Access-Control-Allow-Methods', 'GET');
        res.send();
    });
});

// configuración
app.set('port', process.env.PORT || 3000);

// preparativos
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// rutas
app.use('/api/items/search', require('./services/busqueda'));
app.use('/api/item/', require('./services/item'));



//Inicio de servidor
app.listen(3000, () => {
    console.log(`Servidor en puerto ${app.get('port')}`);
});