const { Router } = require("express");
const fetch = require('node-fetch');
const router = Router();


router.get('/:id', async(req, res) => {

    const item = req.params;
    const articulo = await fetch(`https://api.mercadolibre.com/items/${item.id}`).then(respuesta => respuesta.json());
    const descripcion = await fetch(`https://api.mercadolibre.com/items/${item.id}/description`).then(respuesta => respuesta.json());
    const producto = {
        author: {
            'name': 'Edison',
            'lastname': 'Gamba'
        },
        item: {
            "id": articulo.id,
            "title": articulo.title,
            'price': {
                currency: currency = articulo.currency_id,
                amount: amount = articulo.available_quantity,
                decimals: decimals = 0,
                price: price = articulo.price
            },
            'picture': articulo.thumbnail,
            "condition": articulo.condition,
            "free_shipping": articulo.shipping.free_shipping,
            "sold_quantity": articulo.sold_quantity,
            "description": descripcion.plain_text,
        }
    }

    res.send(producto);

});

module.exports = router;