const { Router } = require("express");
const fetch = require('node-fetch');
const router = Router();

router.get('/:producto', async(req, res) => {

    const item = req.params;
    const producto = JSON.stringify(item.producto);
    const buscar = await fetch(`https://api.mercadolibre.com/sites/MLA/search?q=${producto}&limit=4`).then(response => response.json());
    const items = buscar.results;
    const elementos = [];

    for (let i = 0; i < buscar.results.length; i++) {
        let ob = new Object();
        ob.id = items[i].id;
        ob.title = items[i].title;
        ob.price = {
                currency: currency = (items[i].currency_id) ? items[i].installments.currency_id : '',
                amount: amount = (items[i].installments.amount) ? items[i].installments.amount : '',
                decimals: decimals = 0,
                price: price = items[i].price
            },
            ob.picture = items[i].thumbnail,
            ob.condition = items[i].condition,
            ob.free_shipping = items[i].shipping.free_shipping
        elementos.push(ob);
    }

    const productos = {
        author: {
            'name': 'Edison',
            'lastname': 'Gamba'
        },
        'categorias': [buscar.filters[0].values[0].name],
        'items': elementos
    }
    res.send(productos);

});

module.exports = router;